package com.ua.horishnii.moduletest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.ua.horishnii.example_library.LibMainActivity;

public class MainActivity extends AppCompatActivity {
    private Button mLaunchLibModuleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLaunchLibModuleButton = findViewById(R.id.btn_launch_lib_module);
        mLaunchLibModuleButton.setOnClickListener(clickedView -> {
            Intent intent = new Intent(this, LibMainActivity.class);
            startActivity(intent);
        });
    }
}
